#include "sourceNode.hpp"
#include "utils.hpp"

SourceNode::SourceNode(CXCursor cursor)
{
    // Get the function name
    name = cursorToString(cursor);
}

v8::Local<v8::Object> SourceNode::describe(void)
{
    Nan::EscapableHandleScope scope;

    // Save the name to the description object
    v8::Local<v8::Object> desc = Nan::New<v8::Object>();
    Nan::Set(desc, Nan::New("name").ToLocalChecked(), Nan::New(name.c_str()).ToLocalChecked());

    return scope.Escape(desc);
}
