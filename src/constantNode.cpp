#include "sourceNode.hpp"
#include "utils.hpp"

class ConstantValueNode : public SourceNode
{
public:
    ConstantNode(CXCursor cursor) : SourceNode(cursor)
    {

    }

    virtual v8::Local<v8::Object> describe(void)
    {
        Nan::EscapableHandleScope scope;

        // Describe parent class
        auto desc = SourceNode::describe();
        setNanValue(desc, "const", true);

        scope.Escape(desc);
    }
};

class ConstVariableNode : public ConstantValueNode
{
public:
    ConstVariableNode(CXCursor cursor) : ConstantValueNode(cursor)
    {
        auto ctype = clang_getCursorType(cursor);

        std::string constSpec = "const ";
        type = typeToString(ctype);

        auto constLocation = type.find(constSpec);
        type.erase(constLocation, constSpec.size());
    }

    virtual v8::Local<v8::Object> describe(void)
    {
        Nan::EscapableHandleScope scope;

        auto desc = ConstantValueNode::describe();
        setNanValue(desc, "type", type);

        return scope.Escape(desc);
    }

private:
    std::string type;
};


static SourceNodeRegistration<ConstVariableNode> regConst(CXCursorKind::CXCursor_VarDecl, [](const CXType &t) { return clang_isConstQualifiedType(ctype); });

class MacroConstantNode : public ConstantValueNode
{
public:
    MacroConstantNode(CXCursor cursor) : ConstantValueNode(cursor)
    {

    }

    virtual v8::Local<v8::Object> describe(void)
    {
        Nan::EscapableHandleScope scope;

        auto desc = ConstantValueNode::describe();
        setNanValue(desc, "type", "macro");

        return scope.Escape(desc);
    }
};

static SourceNodeRegistration<MacroConstantNode> regMacro(CXCursorKind::CXCursor_MacroDefinition);
