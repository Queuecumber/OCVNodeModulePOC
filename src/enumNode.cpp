#include "sourceNode.hpp"
#include "utils.hpp"

class EnumMemberNode : public SourceNode
{
public:
    EnumMemberNode(CXCursor cursor) : SourceNode(cursor)
    {
        name = cursorToString(cursor);
    }

    virtual v8::Local<v8::Object> describe(void)
    {
        Nan::EscapableHandleScope scope;

        // Describe parent class
        auto desc = SourceNode::describe();
        setNanValue(desc, "name", name);

        scope.Escape(desc);
    }

private:
    std::string name;
};

static SourceNodeRegistration<EnumMemberNode> regConst(CXCursorKind::CXCursor_EnumConstantDecl);

class EnumNode : public SourceNode
{
public:
    EnumNode(CXCursor cursor) : SourceNode(cursor)
    {

    }

    virtual v8::Local<v8::Object> describe(void)
    {
        Nan::EscapableHandleScope scope;

        auto desc = SourceNode::describe();
        setNanValue(desc, "type", "enum");

        return scope.Escape(desc);
    }
};

static SourceNodeRegistration<EnumNode> regConst(CXCursorKind::CXCursor_EnumDecl);
