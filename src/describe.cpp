#include <nan.h>
#include <clang-c/Index.h>

#include <map>
#include <functional>
#include <algorithm>

#include "utils.hpp"
#include "pluggableDescriber.hpp"

#include <iostream>

CXChildVisitResult visit(CXCursor cursor, CXCursor parent, CXClientData client_data)
{
    Nan::HandleScope scope;

    v8::Local<v8::Object> sourceDescription = *(reinterpret_cast<v8::Local<v8::Object> *>(client_data));

    auto location = clang_getCursorLocation(cursor);

    if(clang_Location_isFromMainFile(location))
    {
        DescriberRegistry &reg = DescriberRegistry::get();
        if(reg.has(cursor.kind))
        {
            auto d = reg[cursor.kind];
            auto r = d(cursor);

            if(r != Nan::Null())
            {
                auto cursorSpelling = clang_getCString(clang_getCursorSpelling(cursor));

                if(cursor.kind == CXCursorKind::CXCursor_FunctionDecl)
                {
                    v8::Local<v8::Array> arr;

                    if(!Nan::Has(sourceDescription, Nan::New(cursorSpelling).ToLocalChecked()).FromJust())
                    {
                        arr = Nan::New<v8::Array>(0);
                        Nan::Set(sourceDescription, Nan::New(cursorSpelling).ToLocalChecked(), arr);
                    }
                    else
                    {
                        arr = Nan::Get(sourceDescription, Nan::New(cursorSpelling).ToLocalChecked()).ToLocalChecked().As<v8::Array>();
                    }

                    V8ArrayPush(arr, r);
                }
                else
                {
                    Nan::Set(sourceDescription, Nan::New(cursorSpelling).ToLocalChecked(), r);
                }
            }
        }
    }

    return CXChildVisitResult::CXChildVisit_Recurse;
}

NAN_METHOD(parseFile)
{
    auto fileNameJsObject = info[0]->ToObject();
    auto fileName = *Nan::Utf8String(fileNameJsObject);

    const char *cflags[3] = {
        "-std=c++11",
        "-I/home/mehrlich/Code/opencv/modules/core/include",
        "/home/mehrlich/Code/opencv/modules/hal/include"
    };

    auto index = clang_createIndex(1, 1);
    auto tu = clang_parseTranslationUnit(index, fileName, cflags, 3, nullptr, 0,
        CXTranslationUnit_Flags::CXTranslationUnit_SkipFunctionBodies |
        CXTranslationUnit_Flags::CXTranslationUnit_DetailedPreprocessingRecord);
    auto rootCursor = clang_getTranslationUnitCursor(tu);

    v8::Local<v8::Object> sourceDescription = Nan::New<v8::Object>();

    clang_visitChildren(rootCursor, visit, &sourceDescription);

    clang_disposeIndex(index);

    info.GetReturnValue().Set(sourceDescription);
}

NAN_MODULE_INIT(init)
{
    Nan::Set(target, Nan::New("parseFile").ToLocalChecked(),
        Nan::GetFunction(Nan::New<v8::FunctionTemplate>(parseFile)).ToLocalChecked());
}

NODE_MODULE(describe, init);
