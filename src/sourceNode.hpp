#pragma once

#include <memory>
#include <array>

#include <clang-c/Index.h>

#include <nan.h>

class SourceNode
{
public:
    virtual v8::Local<v8::Object> describe(void);

protected:
    SourceNode(const CXCursor &cursor);

private:
    std::string name;
    std::vector<std::unique_ptr<SourceNode>> children;
}; 
