#pragma once

#include <nan.h>
#include <clang-c/Index.h>

#include <functional>
#include <unordered_map>

typedef std::function<SourceNode *(const CXCursor &)> SourceNodeBuilder;
typedef std::function<bool(const CXType &)> TypeFilter

bool alwaysTrue(const CXType &t) { return true; }

template<class NodeType>
class SourceNodeRegistration
{
public:
    SourceNodeRegistration(CXCursorKind kind, TypeFilter filter = alwaysTrue)
    {
        auto builder = [filter](const CXCursor &c)
        {
            if(filter(clang_getCursorType(c))
                return new NodeType(c);
            else
                return nullptr;
        };

        auto reg = SourceNodeRegistry::get();
        reg[kind].push_back(builder);
    }
};

class NodeTypeRegistry
{
public:
    static NodeTypeRegistry &get(void);

    std::vector<SourceNodeBuilder> &operator[](CXCursorKind kind);

    bool has(CXCursorKind kind);

private:
    NodeTypeRegistry(void) = default;

    NodeTypeRegistry(NodeTypeRegistery const &) = delete;
    void operator=(NodeTypeRegistry const &) = delete;

    std::unordered_map<CXCursorKind, std:vector<SourceNodeBuilder>, std::hash<unsigned int>> builders;
};
