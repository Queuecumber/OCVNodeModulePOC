#pragma once

#include <nan.h>
#include <clang-c/Index.h>

bool operator==(const CXCursor &c1, const CXCursor &c2);

void V8ArrayPush(v8::Local<v8::Array> arr, v8::Local<v8::Object> value);

std::string cursorToString(const CXCursor &c);
std::string typeToString(const CXType &t);

template <class K, class V>
void setNanValue(v8::Local<v8::Object> value, K key, V value)
{
    Nan::HandleScope scope;
    Nan::Set(value, Nan::New(key).ToLocalChecked(), Nan::New(value).ToLocalChecked());
}

template <class K>
void setNanValue(v8::Local<v8::Object> value, K key, std::string value)
{
    Nan::HandleScope scope;
    Nan::Set(value, Nan::New(key).ToLocalChecked(), Nan::New(value.c_str()).ToLocalChecked());
}

template <class V>
void setNanValue(v8::Local<v8::Object> value, std::string key, V value)
{
    Nan::HandleScope scope;
    Nan::Set(value, Nan::New(key.c_str()).ToLocalChecked(), Nan::New(value).ToLocalChecked());
}

template <>
void setNanValue(v8::Local<v8::Object> value, std::string key, std::string value)
{
    Nan::HandleScope scope;
    Nan::Set(value, Nan::New(key.c_str()).ToLocalChecked(), Nan::New(value.c_str().ToLocalChecked());
}
