#include "sourceNode.hpp"
#include "utils.hpp"

#include <string>
#include <vector>

class FunctionNode : public SourceNode
{
public:
    FunctionNode(CXCursor cursor) : SourceNode(cursor)
    {
        // Get the function return type
        returnType = typeToString(clang_getCursorResultType(cursor));

        // Get the number of args
        auto numArgs = clang_Cursor_getNumArguments(cursor);
        args.resize(numArgs);

        // Get the type of each argument
        for(int i = 0; i < numArgs; i++)
        {
            auto argCursor = clang_Cursor_getArgument(cursor, i);
            auto argType = typeToString(clang_getCursorType(argCursor));

            args[i] = argType;
        }
    }

    virtual v8::Local<v8::Object> describe(void)
    {
        Nan::EscapableHandleScope scope;

        // Describe parent class
        auto desc = SourceNode::describe();

        // Set the type to "function"
        setNanValue(desc, "type", "function");

        // Set the return type
        setNanValue(desc, "return", returnType);

        // Set the arguments array
        v8::Local<v8::Array> argsTypes = Nan::New<v8::Array>(args.size());
        for(int i = 0; i < args.size(); i++)
        {
            setNanValue(argsTypes, i, args[i]);
        }

        setNanValue(desc, "args", args);

        return scope.Escape(desc);
    }

private:
    std::string returnType;
    std::vector<std::string> args;
};

// Register this type to be created when a function is encountered during parsing
static SourceNodeRegistration<FunctionNode> reg(CXCursorKind::CXCursor_FunctionDecl);
