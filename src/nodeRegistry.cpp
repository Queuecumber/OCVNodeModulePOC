#include "nodeRegistry.hpp"

NodeTypeRegistry &NodeTypeRegistry::get(void)
{
    static NodeTypeRegistry inst;
    return inst;
}

std::vector<SourceNodeBuilder> &NodeTypeRegistry::operator[](CXCursorKind kind)
{
    return builders[kind];
}

bool NodeTypeRegistry::has(CXCursorKind kind)
{
    return builders.find(kind) != builders.end();
}
