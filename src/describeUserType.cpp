#include "pluggableDescriber.hpp"
#include "utils.hpp"

#include <unordered_map>

class DescribeMember
{
public:
    virtual v8::Local<v8::Object> operator ()(CXCursor cursor) = 0;

    void setDescription(CXCursor parentCursor, v8::Local<v8::Object> desc)
    {
        Nan::HandleScope scope;

        classDescriptions[parentCursor].Reset();
        classDescriptions[parentCursor] = desc;
    }

    ~DescribeMember()
    {
        for(auto p : classDescriptions)
        {
            p.second.Reset();
        }

        classDescriptions.clear();
    }

protected:
    std::unordered_map<CXCursor, Nan::Persistent<v8::Object, v8::CopyablePersistentTraits<v8::Object>>, hash_cursor> classDescriptions;
};

class DescribeMemberVariable : public DescribeMember
{
public:
    virtual v8::Local<v8::Object> operator ()(CXCursor cursor)
    {
        Nan::HandleScope scope;

        auto parentCursor = clang_getCursorSemanticParent(cursor);

        if(classDescriptions.find(parentCursor) != classDescriptions.end())
        {
            auto access = clang_getCXXAccessSpecifier(cursor);

            if(access == CX_CXXAccessSpecifier::CX_CXXPublic)
            {
                v8::Local<v8::Object> desc = Nan::New<v8::Object>();

                auto typeName = clang_getCString(clang_getTypeSpelling(clang_getCursorType(cursor)));
                Nan::Set(desc, Nan::New("type").ToLocalChecked(), Nan::New(typeName).ToLocalChecked());

                auto name = clang_getCString(clang_getCursorSpelling(cursor));

                auto parentDescription = Nan::New(classDescriptions[parentCursor]);

                v8::Local<v8::Object> properties = Nan::Get(parentDescription, Nan::New("properties").ToLocalChecked()).ToLocalChecked().As<v8::Object>();
                Nan::Set(properties, Nan::New(name).ToLocalChecked(), desc);
            }
        }

        return Nan::Null().As<v8::Object>();
    }
};

static DescribeMemberVariable dmv;
static PluggableDescriber regDmf(CXCursorKind::CXCursor_FieldDecl, std::ref(dmv));

class DescribeMemberFunction : public DescribeMember
{
public:
    virtual v8::Local<v8::Object> operator ()(CXCursor cursor)
    {
        Nan::HandleScope scope;

        auto parentCursor = clang_getCursorSemanticParent(cursor);

        if(classDescriptions.find(parentCursor) != classDescriptions.end())
        {
            auto access = clang_getCXXAccessSpecifier(cursor);

            if(access == CX_CXXAccessSpecifier::CX_CXXPublic)
            {
                v8::Local<v8::Object> desc = Nan::New<v8::Object>();

                auto name = clang_getCString(clang_getCursorSpelling(cursor));

                std::string operatorPrefix = "operator";
                if(cursor.kind == CXCursorKind::CXCursor_Constructor)
                    Nan::Set(desc, Nan::New("type").ToLocalChecked(), Nan::New("constructor").ToLocalChecked());
                else if(std::equal(operatorPrefix.begin(), operatorPrefix.end(), std::string(name).begin()))
                    Nan::Set(desc, Nan::New("type").ToLocalChecked(), Nan::New("operator").ToLocalChecked());
                else
                    Nan::Set(desc, Nan::New("type").ToLocalChecked(), Nan::New("function").ToLocalChecked());

                if(cursor.kind == CXCursorKind::CXCursor_CXXMethod)
                {
                    auto returnType = clang_getTypeSpelling(clang_getCursorResultType(cursor));
                    Nan::Set(desc, Nan::New("return").ToLocalChecked(), Nan::New(clang_getCString(returnType)).ToLocalChecked());
                }

                auto numArgs = clang_Cursor_getNumArguments(cursor);

                v8::Local<v8::Array> args = Nan::New<v8::Array>(numArgs);
                for(int i = 0; i < numArgs; i++)
                {
                    auto argCursor = clang_Cursor_getArgument(cursor, i);
                    auto argType = clang_getTypeSpelling(clang_getCursorType(argCursor));

                    Nan::Set(args, i, Nan::New(clang_getCString(argType)).ToLocalChecked());
                }

                Nan::Set(desc, Nan::New("args").ToLocalChecked(), args);

                auto parentDescription = Nan::New(classDescriptions[parentCursor]);

                v8::Local<v8::Object> properties;
                properties = Nan::Get(parentDescription, Nan::New("methods").ToLocalChecked()).ToLocalChecked().As<v8::Object>();

                v8::Local<v8::Array> container;

                if(!Nan::Has(properties, Nan::New(name).ToLocalChecked()).FromJust())
                {
                    container = Nan::New<v8::Array>(0);
                    Nan::Set(properties, Nan::New(name).ToLocalChecked(), container);
                }
                else
                {
                    container = Nan::Get(properties, Nan::New(name).ToLocalChecked()).ToLocalChecked().As<v8::Array>();
                }

                V8ArrayPush(container, desc);
            }
        }

        return Nan::Null().As<v8::Object>();
    }
};

static DescribeMemberFunction dmf;
static PluggableDescriber regDmv(CXCursorKind::CXCursor_CXXMethod, std::ref(dmf));
static PluggableDescriber regCs(CXCursorKind::CXCursor_Constructor, std::ref(dmf));

class DescribeUserType
{
public:
    DescribeUserType(std::string _type)
        : type(_type)
    {

    }

    v8::Local<v8::Object> operator ()(CXCursor cursor)
    {
        Nan::EscapableHandleScope scope;

        v8::Local<v8::Object> desc = Nan::New<v8::Object>();

        Nan::Set(desc, Nan::New("type").ToLocalChecked(), Nan::New(type.c_str()).ToLocalChecked());

        Nan::Set(desc, Nan::New("methods").ToLocalChecked(), Nan::New<v8::Object>());
        Nan::Set(desc, Nan::New("properties").ToLocalChecked(), Nan::New<v8::Object>());

        dmv.setDescription(cursor, desc);
        dmf.setDescription(cursor, desc);

        return scope.Escape(desc);
    }

private:
    std::string type;
};

static DescribeUserType dc("class");
static PluggableDescriber regC(CXCursorKind::CXCursor_ClassDecl, std::ref(dc));

static DescribeUserType ds("struct");
static PluggableDescriber regS(CXCursorKind::CXCursor_StructDecl, std::ref(ds));

static DescribeUserType du("union");
static PluggableDescriber regU(CXCursorKind::CXCursor_UnionDecl, std::ref(du));
