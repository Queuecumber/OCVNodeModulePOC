#include "utils.hpp"

bool operator==(const CXCursor &c1, const CXCursor &c2)
{
    return clang_equalCursors(c1, c2);
}

void V8ArrayPush(v8::Local<v8::Array> arr, v8::Local<v8::Object> value)
{
    Nan::HandleScope scope;

    auto len = arr->Length();

    Nan::Set(arr, Nan::New("Length").ToLocalChecked(), Nan::New(len+1));
    Nan::Set(arr, len, value);
}

std::string cursorToString(const CXCursor &c)
{
    return clang_getCString(clang_getCursorSpelling(c));
}

std::string typeToString(const CXType &t)
{
    return clang_getCString(clang_getTypeSpelling(t));
}
