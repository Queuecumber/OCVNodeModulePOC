{
    "targets": [{
        "target_name": "describe",

        "sources": [
            "src/pluggableDescriber.cpp",

            "src/describe.cpp",

            "src/utils.cpp",

            "src/describeFunction.cpp",
            "src/describeUserType.cpp",
            "src/describeConstant.cpp"
        ],

        "libraries": [
            "-lclang"
        ],

        "include_dirs": [
            "<!(node -e \"require('nan')\")"
        ],

        "cflags!": ["-fno-exceptions"],
        "clags_cc!": ["-fno-rtti", "-fno-exceptions"],

        "cflags_cc": ["-Wall", "-std=c++1y", "-fexceptions"]
    }]
}
